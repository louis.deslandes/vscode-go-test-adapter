import { WorkspaceConfiguration, ConfigurationScope, workspace, ConfigurationChangeEvent, Event } from 'vscode';

interface ConfigurationConstructor<T> {
    new (scope: ConfigurationScope): T;
}

interface SubConfigurationConstructor<T> {
    new (source: WorkspaceConfiguration, name: string | symbol): T;
}

interface Get {
    get<T>(name: string): T | undefined;
    get<T>(name: string, defaultValue: T): T;
}

abstract class BaseConfiguration {
    public static readonly section: string;
    public readonly source: WorkspaceConfiguration;

    static get<T extends BaseConfiguration>(this: ConfigurationConstructor<T>, scope: ConfigurationScope): T {
        return new this(scope)
    }

    static get onDidChange(): Event<ConfigurationChangeEvent> {
        return l => workspace.onDidChangeConfiguration(e => {
            if (e.affectsConfiguration(this.section)) l(e)
        })
    }

    constructor(
        public readonly scope: ConfigurationScope
    ) {
        this.source = workspace.getConfiguration(Configuration.section, scope)
    }

    get<T>(name: string): T | undefined;
    get<T>(name: string, defaultValue: T): T;
    get<T>(name: string, defaultValue?: T) {
        if (arguments.length == 1)
            return this.source.get<T>(name)
        else
            return this.source.get<T>(name, defaultValue!)
    }
}

abstract class BaseSubConfiguration {
    constructor(
        public readonly source: WorkspaceConfiguration,
        public readonly name: string | symbol
    ) { }


    get<T>(name: string): T | undefined;
    get<T>(name: string, defaultValue: T): T;
    get<T>(name: string, defaultValue?: T) {
        if (arguments.length == 1)
            return this.source.get<T>(`${String(this.name)}.${name}`)
        else
            return this.source.get<T>(`${String(this.name)}.${name}`, defaultValue!)
    }
}

function get<T>(target: any, property: string | symbol, descriptor: TypedPropertyDescriptor<T>): void;
function get<T>(opts: { name?: string, default?: any }): MethodDecorator;
function get<T>(opts: { name?: string, default?: any } | any = {}, property?: string | symbol, descriptor?: TypedPropertyDescriptor<T>): MethodDecorator | TypedPropertyDescriptor<T> {
    if (property && descriptor) {
        const target = opts
        opts = {}
        return define(target, property, descriptor)
    }

    return <MethodDecorator>define

    function define(target: any, property: string | symbol, descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> {
        const { name = property, default: defaultValue } = opts
        let { get = () => void 0 } = Object.getOwnPropertyDescriptor(target, property) || {}

        if ('defaultValue' in opts)
            get = () => defaultValue

        return { get: getConfigValue }

        function getConfigValue(this: Get) {
            return this.get<T>(name, get.call(this))
        }
    }
}

function sub<V extends BaseSubConfiguration>(type: SubConfigurationConstructor<V>, opts: { name?: string } = {}): PropertyDecorator {
    return function<T>(target: any, property: string | symbol): TypedPropertyDescriptor<T> {
        const { name = property } = opts
        // @ts-ignore
        return { get: getSubConfiguration }

        function getSubConfiguration(this: BaseConfiguration) {
            return new type(this.source, name)
        }
    }
}

export class Profiler extends BaseSubConfiguration {
    @get get showCodeLens() { return true }
}

export class RunPackage extends BaseSubConfiguration {
    @get get excludeBenchmarks() { return true }
}

export class Configuration extends BaseConfiguration {
    static readonly section = 'goTestExplorer';

    @get get logpanel() { return false }
    @get get logfile() { return '' }
    @get get showFiles() { return 'always' }
    @sub(Profiler) profiler!: Profiler;
    @sub(RunPackage) runPackage!: RunPackage;
}

export class GoAlternateTools extends BaseSubConfiguration {
    @get get go(): string | undefined { return }
}

export class GoConfiguration extends BaseConfiguration {
    static readonly section = 'go';

    @get get testTags() { return '' }
    @get get buildTags() { return 'x' }
    @get get testFlags(): string[] | undefined { return }
    @get get buildFlags(): string[] | undefined { return }
    @get get testTimeout() { return 30 }
    @get get toolsEnvVars(): {[key: string]: any} | undefined { return }
    @get get testEnvVars(): {[key: string]: any} | undefined { return }

    @sub(GoAlternateTools) alternateTools!: GoAlternateTools;
}
