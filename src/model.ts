import * as vscode from 'vscode';
import * as path from 'path';
import { TextDecoder } from 'util';

export interface Lookup<V> {
    [key: string]: V
}

export class Module {
    private static cache: Lookup<Promise<Module | null>> = {}

    static resolve(uri: vscode.Uri): Promise<Module | null> {
        const key = uri.toString()
        if (key in this.cache)
            return this.cache[key]

        return this.cache[key] = this.resolveUncached(uri)
    }

    static invalidate(uri: vscode.Uri): void {
        const key = uri.toString()
        for (const path in this.cache)
            if (path.startsWith(key))
                delete this.cache[path]
    }

    private static async resolveUncached(uri: vscode.Uri): Promise<Module | null> {
        while (path.parse(uri.fsPath).name != '') {
            try {
                const b = await vscode.workspace.fs.readFile(uri.with({ path: path.join(uri.fsPath, 'go.mod') }))
                const m = new TextDecoder("utf-8").decode(b).match(/^module (?<name>[^\s\r\n]+)/)
                if (m?.groups?.name)
                    return new Module(m.groups.name, uri)
                else
                    return null
            } catch (error) {
                if (error instanceof vscode.FileSystemError)
                    uri = uri.with({ path: path.dirname(uri.fsPath) })
                else
                    throw new Error(error)
            }
        }

        return null
    }

    constructor(
        public readonly name: string,
        public readonly uri: vscode.Uri,
    ) { }
}

export class Package {
    static async resolve(uri: vscode.Uri, ws: vscode.WorkspaceFolder): Promise<Package> {
        const dir = path.extname(uri.fsPath) == '' ? uri : uri.with({ path: path.dirname(uri.path) })
        const mod = await Module.resolve(uri)
        const root = path.normalize((mod?.uri || ws.uri).fsPath)

        const fsDir = path.normalize(dir.fsPath)
        if (fsDir != root && !fsDir.startsWith(root + path.sep))
            return new Package(mod, null, dir)

        const pkgPath = fsDir.slice(root.length+1).replace(/\\/g, '/')
        return new Package(mod, pkgPath, dir)
    }

    constructor(
        public readonly module: Module | null,
        public readonly path: string | null,
        public readonly uri: vscode.Uri,
    ) { }

    get isValid() { return this.path != null }

    get name() {
        if (this.module && this.path)
            return `${this.module.name}/${this.path}`

        if (this.module)
            return this.module.name

        // TODO handle GOPATH mode
        return this.uri.fsPath
    }

    get id() {
        if (!this.isValid)
            throw new Error('an invalid package does not have an ID')

        return `Go·${this.name}`
    }
}

export class File {
    static async resolve(uri: vscode.Uri, ws: vscode.WorkspaceFolder): Promise<File> {
        const pkg = await Package.resolve(uri, ws)
        const name = path.basename(uri.fsPath)
        return new File(pkg, name, uri)
    }

    constructor(
        public readonly pkg: Package,
        public readonly name: string,
        public readonly uri: vscode.Uri,
    ) { }

    get id() { return `${this.pkg.id}·${this.name}` }

    async symbols(): Promise<Symbol[]> {
        const doc = await vscode.workspace.openTextDocument(this.uri)
        const vsSyms: vscode.SymbolInformation[] | undefined = await vscode.commands.executeCommand('vscode.executeDocumentSymbolProvider', this.uri)

        const symbols: Symbol[] = []
        this.processSymbols(doc, vsSyms, symbols)
        return symbols
    }

    private async processSymbols(doc: vscode.TextDocument, vsSyms: vscode.SymbolInformation[] | undefined, symbols: Symbol[]): Promise<void> {
        if (!vsSyms) return

        for (const sym of vsSyms) {
            // @ts-ignore
            if (sym.kind != vscode.SymbolKind.Function || sym.name == 'TestMain' || /\*testing.M\)/.test(sym.detail)) {
                // @ts-ignore
                this.processSymbols(doc, sym.children, symbols);
                continue
            }

            const m = sym.name.match(/^(Test|Example|Benchmark)/)
            if (!m)
                continue

            // @ts-ignore
            const type = SymbolType[m[1]]
            const text = doc.getText(sym.location.range)
            symbols.push(new Symbol(this, sym, text, type))
        }
    }
}

export class Symbol {
    constructor(
        public readonly file: File,
        public readonly info: vscode.SymbolInformation,
        public readonly text: string,
        public readonly type: SymbolType,
    ) { }

    get name() { return this.info.name }
    get id() { return `${this.file.id}·${this.name}` }
}

export enum SymbolType {
    Unknown,
    Test,
    Example,
    Benchmark
}